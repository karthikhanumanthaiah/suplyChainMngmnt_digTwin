# %% [markdown]
# ## Initialization
# Create a priority queue for each node
# Priority is based on start and end time ( assigne time segments to item)
# 
# based on optimization value insert item to queue
#     While calculating optimization, initial values of depended variables would be next value in the queue
# 
# Keep adding items to the queue until there is a confilict
# Conflict resolution : 
#     - Find top 10 optimal soulution for item
#         - If only one optimal solution existis -> go based on priority
#         - If no optimal solution existis -> increment delivery time to find next best solution
#     - Update priority based on below rule 
#         - items which can arrive early
#         - items which has lower priority (Combination of inital + calculated priority)
#     
#  Items that are picked for processing cannot be changed (i.e last itme in queue)  
#  
#  Input : 
# Item Id, Planned Delivery Time, Inital Priority, Route{Node, Processing Time, Transport Time}
# 
# Output : Digital twin with Queue value assigned to each node

# %%
import pandas as pd
import datetime
# optimizer
from ortools.linear_solver import pywraplp
import math
# scheduler
import threading
import time
import schedule
from kafka import KafkaConsumer
from datetime import datetime, timedelta
import math

import faulthandler

faulthandler.enable()

# %%
processingCapacity = 1
get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]

# %%
class PriorityQue(object):
    def __init__(self):
        self.priorityQue = pd.DataFrame(columns=['priority', 'item' ])
    def insert(self, data):
        priority = data[0]
        if (priority not in self.priorityQue.priority) or (
            priority in self.priorityQue.priority and len(get_indexes(priority,self.priorityQue.priority.values)) < processingCapacity):
                temp = pd.DataFrame(data=[data], columns=self.priorityQue.columns)
                self.priorityQue = self.priorityQue.append(temp, ignore_index=True)
                self.priorityQue = self.priorityQue.sort_index()
        else:
            return self.priorityQue.loc[priority]
    def remove(self,item):
        if item in self.priorityQue.item.values:
            self.priorityQue = self.priorityQue.drop(self.priorityQue.index[self.priorityQue['item'] == item])
    def checkPriorityAvailable(self, priority):
        return self.priorityQue[self.priorityQue.priority == priority]
    def findItemByIndex(self, item, priority):
        # if priority in self.priorityQue.priority:
        return self.priorityQue[(self.priorityQue.priority == priority) & (self.priorityQue.item == item)]
        # return self.priorityQue[(self.priorityQue.priority == priority)]
        # if index in self.priorityQue.priority:
        #     return self.priorityQue.loc[index, 'item']
        # else:
        #     return None

# %%
class SupplyChainNode(object):
    def __init__(self, id):
        self.id = id
        self.priorityQueue = PriorityQue()
    def addPriority(self, priority):
        return self.priorityQueue.insert(priority)
    def removePriority(self, itemId):
        self.priorityQueue.remove(itemId)



# %%
class DigitalTwinNode(object):
    def __init__(self, id, processingTime, transportTime, parentNodeRef):
        self.id = id
        self.processingTime = processingTime
        self.transportTime = transportTime
        self.markAsComplete = False
        self.link = None
        self.revLink = None
        self.queVar = None
        self.queVal = None
        self.optimalQueValue = None
        self.parentNodeRef = parentNodeRef
    def linkNode(self, node):
        self.link = node 
        node.revLink = self
    def setPlannedStartTime(self, startTime):
        self.plannedStartTime = startTime
    def markAsComplete(self):
        self.markAsComplete = True
        self.completedTime = datetime.datetime.now()
    def findNode(self, nodeId):
        if id == nodeId:
            return self
        elif self.link != Null:
            self.link.findNode(nodeId)
        else:
            return Null
    def findLastActiveNode(self):
        if self.markAsComplete == False:
            return self
        elif self.link != Null:
            return self.link.findLastActiveNode()
        else:
            return Null
    def commit(self):
        self.optimalQueValue = self.queVal
    def revert(self):
        self.queVal = self.optimalQueValue

# %%
class DigitalTwin(object):
    def __init__(self, itemID, transportRoute, plannedDeliveryDate, priority):
        self.itemID = itemID
        self.transportRoute = transportRoute
        self.initialPriority = priority
        self.dynamicPriority = 0 # gets incremented everytime item is rescheduled by either higher pri item or late delivery item
        self.plannedDeliveryDate = plannedDeliveryDate
        self.arrivedAtDestination = False
    def updateNodePlannedStartTime(self, nodeId, startTime):
        node = stransportRouteansportRoute.findNode(nodeId)
        if node != None:
            node.plannedStartTime = startTime
    def markNodeAsComplete(self):
        globalTimeline.removeScheduleByTag(str(self.transportRoute.id)+str(self.itemID))
        self.transportRoute.markAsComplete = True
        self.transportRoute.parentNodeRef.priorityQueue.remove(self) # remove priority
        if self.transportRoute.link == None:
            self.arrivedAtDestination = True
            print("arrived at destination")
            # globalTimeline.
        else:
            self.transportRoute = self.transportRoute.link
            self.transportRoute.revLink = None # reset rev link
            supplyChainSimOptEngine = SupplyChainSimOptEngine()
            supplyChainSimOptEngine.onNodeCompletion(self)
    def commitAllNode(self):
        node = self.transportRoute
        while node != None:
            node.parentNodeRef.priorityQueue.remove(self) # remove priorty
            node.commit()
            node.parentNodeRef.priorityQueue.insert([abs(int(node.optimalQueValue)), self])
            node = node.link

# %%
class SupplyChainOptimizer(object):
    def __init__(self, digitalTwin):
        self.solver = pywraplp.Solver.CreateSolver('SCIP')
        self.digitalTwin = digitalTwin
        self.declareVariables()
        self.defineConstraints()
        self.defineObjective()
    def declareVariables(self):
        infinity = self.solver.infinity()
        node = self.digitalTwin.transportRoute
        while node != None:
            lb = 0
            if node.queVal != None:
                lb = node.queVal
            node.queVar = self.solver.IntVar(lb, infinity, 'x[%s]' % node.id)
            node = node.link
    def defineConstraints(self):
        totalProcTransTime = 0.0
        node = self.digitalTwin.transportRoute
        time_limit_contraint = [] 

        duration = self.digitalTwin.plannedDeliveryDate - datetime.now()
        duration_in_s = math.ceil(duration.total_seconds())

        while node != None:
            totalProcTransTime += (node.processingTime + node.transportTime)
            lastNode = node.revLink
            #   Di = Pi * Xi
            time_limit_contraint.append(node.processingTime * node.queVar)
            
            constraint_expr = []
            # Pi*Qi >= sum(P(i-1) + T(i-1) + D(i-1)) | i -> for all prev node
            # if lastNode != None:
            #     while lastNode != None:
            #         constraint_expr.append(lastNode.processingTime + lastNode.transportTime + 
            #                                lastNode.processingTime * lastNode.queVar)
            #         lastNode = lastNode.revLink
            #     self.solver.Add(sum(constraint_expr) <= (node.processingTime * node.queVar))
            
            # Pi * Qi <= E - sum(p(i-1) + T(i-1) + D(i-1))
            if lastNode != None:
                while lastNode != None:
                    constraint_expr.append(lastNode.processingTime + lastNode.transportTime + 
                                           lastNode.processingTime * lastNode.queVar)
                    lastNode = lastNode.revLink
                self.solver.Add((node.processingTime * node.queVar) <= (duration_in_s - sum(constraint_expr)))

            node = node.link
            
        #   D1 + D2 + ... + Di <= E - sum(Pi + Ti) | i -> for all remaining node
        self.solver.Add(sum(time_limit_contraint) <= (duration_in_s - totalProcTransTime))
        
    def defineObjective(self):
        node = self.digitalTwin.transportRoute
        objective_expr = []
        while node != None:
            objective_expr.append(node.processingTime + node.transportTime + (node.processingTime * node.queVar))
            node = node.link
        self.solver.Minimize(self.solver.Sum(objective_expr))
    def optimize(self, digitalTwin, commit=True):
        status = self.solver.Solve()
        if status == pywraplp.Solver.OPTIMAL:
            print('itemId',digitalTwin.itemID,'Objective value =', self.solver.Objective().Value(), "( ",self.solver.wall_time(), "ms )")
            node = self.digitalTwin.transportRoute
            while node != None:
                print(node.queVar.name(), '=', node.queVar.solution_value())
                node.queVal =  node.queVar.solution_value()
                node = node.link
            return True
        else:
            return False
    def nextOptimalSolution(self, digitalTwin):
        node = digitalTwin.transportRoute
        solution = []
        while node != None:
            node.queVar.SetLb(node.queVal + 1)
#             n = n.link
            node = node.link
        o = self.optimize(digitalTwin)
        return o
    def nextOptimalSolutionWithFixedValNodes(self, nodes):
        node = self.digitalTwin.transportRoute
        increment = False # To make sure not to increment lower nodes (this would result in conflicting solution)
        while node != None:
            node.commit()
            if (node in nodes):
                node.queVar.SetLb(node.queVal)
                node.queVar.SetUb(node.queVal)
                increment = True
            elif increment == True:
                node.queVar.SetLb(node.queVal + 1)
            node = node.link
        optimalVal = self.optimize()
        if optimalVal == False:
            # revert changes
            node = self.digitalTwin.transportRoute
            while node != None:
                node.revert()
                node = node.link
        return optimalVal
    def nextOptimalSolutionWithFixedValOtherThanNodes(self, node):
        node = self.digitalTwin.transportRoute
        # increment = False # To make sure not to increment lower nodes (this would result in conflicting solution)
        while node != None:
            if (node.id == node.id):
                node.queVar.SetLb(node.queVal + 1)
            node = node.link

        optimalVal = self.optimize(self.digitalTwin)
        if optimalVal == False:
            # revert changes
            node = self.digitalTwin.transportRoute
            while node != None:
                node.revert()
                node = node.link
        return optimalVal
    def nextOptimalDeliveryTimeline(self, digitalTwin):
        infinity = self.solver.infinity()
        initialDeliveryDate = self.digitalTwin.plannedDeliveryDate
        self.digitalTwin.plannedDeliveryDate = infinity
        for constraints in self.solver.constraints():
            constraints.SetUb(infinity)
        o = self.nextOptimalSolution(digitalTwin)
        node = digitalTwin.transportRoute
        timeOffset = 0
        while node != None:
            timeOffset += (node.processingTime + node.transportTime + (node.processingTime * node.queVal))
            node = node.link
        self.digitalTwin.plannedDeliveryDate = initialDeliveryDate  + timedelta(seconds=timeOffset)
        
            

# %%
# optimizer = SupplyChainOptimizer(600, digitalTwin=dt)
# optimizer.optimize()
# optimizer.nextOptimalSolution()
# optimizer.nextOptimalSolutionWithFixedVal([n2])

# %%
cease_continuous_run = threading.Event()

class SchedulerThread(threading.Thread):
    @classmethod
    def run(cls):
        while not cease_continuous_run.is_set():
            schedule.run_pending()
            time.sleep(1)

class Scheduler(object):
    def __init__(self):
        continuous_thread = SchedulerThread()
        continuous_thread.start()
    def job(self, digitalTwin):
        simOptEngine = SupplyChainSimOptEngine()
        simOptEngine.checkIfOptimalSolutionExist(digitalTwin)
        return schedule.CancelJob
    def scheduleJobForNextNSec(self, sec, digitalTwin, tag):
        schedule.every(sec).seconds.do(self.job, digitalTwin=digitalTwin).tag(tag)
    def removeScheduleByTag(self, tag):
        schedule.clear(tag)
    def stopScheduler(self):
        cease_continuous_run.set()
        

# %%
# Create Global Timeline Instance
globalTimeline = Scheduler()

# %%
class SupplyChainSimulator(object):
    # def __init__(self, digitalTwin):
    #     self.digitalTwin = digitalTwin
    def __init__(self):
        self.conflictingNodes = []
    # conflictingNodes = []
    def simulate(self, digitalTwin, optimizer, alreadyLate=False):
        # keep track of all optimal solution sort by conflicts
        conflicts = self.numOfConflits(digitalTwin)
        if conflicts == 0:
            digitalTwin.commitAllNode()
            return True
        else:
            # makesure you clear conflicting nodes
            if alreadyLate == True:
                optimalSolution = False
            else:
                optimalSolution = optimizer.nextOptimalSolution(digitalTwin)
            if optimalSolution == False:
                print("conflict resolution for item ", digitalTwin.itemID)
                resResult = self.conflictResolution()
                if resResult == 0:
                    return True
                elif resResult == 1:
                    self.simulate(digitalTwin, optimizer)
                else:
                    return False
            else:
                return self.simulate(digitalTwin, optimizer=optimizer)    
    def conflictResolution(self):
        # start with least conlicts
        # sort by least number of conflicts
        self.conflictingNodes.sort(key=lambda x: len(x))
        for index, conflictingItems in enumerate(self.conflictingNodes):
            updatedConflictingNode = []
            for conflictingNode in conflictingItems:
                # node
                node = conflictingNode["node"]
                itemIndexes = conflictingNode["itemIndexes"]
                priority = conflictingNode["priority"]
                for itemIndex in itemIndexes:
                    # digitalTwin = itemIndex
                    # digitalTwinRefAtIndex= node.priorityQueue.findItemByIndex(itemIndex, priority)
                    for _, priorityQueueItem in node.priorityQueue.findItemByIndex(itemIndex, priority).iterrows():
                        digitalTwin = priorityQueueItem['item']
                        # if digitalTwinRefAtIndex.shape[0] > 0 :
                        optimizer = SupplyChainOptimizer(digitalTwin)
                        optimalVal = optimizer.nextOptimalSolutionWithFixedValOtherThanNodes(node=node)

                        if optimalVal == False:
                            print("conflict res : no optimal value found for item ", digitalTwin.itemID)
                            updatedConflictingNode.append(conflictingNode)
                        else:
                            # update timeline for optimal solution
                            print("conflict res : rescheduling for item ", digitalTwin.itemID)
                            digitalTwin.commitAllNode()
                            globalTimeline.removeScheduleByTag(str(node.id)+str(digitalTwin.itemID))
                            SupplyChainSimOptEngine().updateTimeline(digitalTwin)

            if len(updatedConflictingNode) == 0:
                return 0 # optimal solution exist
            else:
                self.conflictingNodes[index] = updatedConflictingNode
                
        return 2 # no optimal solution, switch to priority based optimization
    def numOfConflits(self, digitalTwin):
        node = digitalTwin.transportRoute
        conflicts = 0
        dt = []
        while node != None:
            conflictNode = {}
            # node.queVal
            priorityItems = node.parentNodeRef.priorityQueue.checkPriorityAvailable(node.queVal)
            if priorityItems.shape[0] > 0:
                conflicts += 1
                conflictNode["priority"] = node.queVal
                conflictNode["node"] = node.parentNodeRef
                conflictNode["itemIndexes"] = priorityItems["item"]
                dt.append(conflictNode)
            node = node.link
        if len(dt) > 0:
            self.conflictingNodes.append(dt)
        return conflicts
# %%
class SupplyChainSimOptEngine(object):
    def onTwinCreate(self, digitalTwin):
        optimizer = SupplyChainOptimizer(digitalTwin)
        SupplyChainSimulator.conflictingNodes =[]
        simulator = SupplyChainSimulator()

        optimalSoultion = optimizer.optimize(digitalTwin)
        if optimalSoultion == True:
            optimalSoultion = simulator.simulate(digitalTwin, optimizer=optimizer)

        self.postSimulation(optimalSoultion=optimalSoultion, optimizer=optimizer, digitalTwin=digitalTwin)
    # return None if optimal solution exist
    # return Extended delivery time if no optimal solution exist
    def checkIfOptimalSolutionExist(self, digitalTwin):
        node = digitalTwin.transportRoute
        optimizer = SupplyChainOptimizer(digitalTwin)
        simulator = SupplyChainSimulator()

        optimalSoultion = optimizer.nextOptimalSolution(digitalTwin)
        if optimalSoultion == True:
            optimalSoultion = simulator.simulate(digitalTwin, optimizer=optimizer)
        
        self.postSimulation(optimalSoultion=optimalSoultion, optimizer=optimizer, digitalTwin=digitalTwin)
        return None
    def onNodeCompletion(self, digitalTwin):
        optimizer = SupplyChainOptimizer(digitalTwin)
        simulator = SupplyChainSimulator()

        optimalSoultion = optimizer.optimize(digitalTwin)
        if optimalSoultion == True:
            optimalSoultion = simulator.simulate(digitalTwin, optimizer=optimizer)

        self.postSimulation(optimalSoultion=optimalSoultion, optimizer=optimizer, digitalTwin=digitalTwin)
    def postSimulation(self, optimalSoultion, optimizer, digitalTwin):
        simulator = SupplyChainSimulator()

        if(optimalSoultion == False):
            optimizer.nextOptimalDeliveryTimeline(digitalTwin=digitalTwin)
            print("no optimal solution found for item ",digitalTwin.itemID,", triggering action..., bumping delivery date to", digitalTwin.plannedDeliveryDate)
            optimalSoultion = simulator.simulate(digitalTwin, optimizer=optimizer, alreadyLate=True)
        self.updateTimeline(digitalTwin)
        # else:
            
        #     
            
        #     # globalTimeline.stopScheduler()
        #     self.updateTimeline(digitalTwin)

    def updateTimeline(self, digitalTwin):
        nextOpenNode = digitalTwin.transportRoute
        nextSchedule = int(nextOpenNode.processingTime) + int(nextOpenNode.transportTime) + int(nextOpenNode.processingTime * nextOpenNode.queVal)
        print("scheduling for item ", digitalTwin.itemID, "at ", nextSchedule)
        globalTimeline.scheduleJobForNextNSec(nextSchedule, digitalTwin, (str(nextOpenNode.id) + str(digitalTwin.itemID)))

# %% [markdown]
# ## Twin generator
# 1. Create route for item
# 2. Create instance of digitial twin 
# 
# ## Update weight
# 1. update weights (from regression engine)
# 2. run optimizer initially to update priority (TODO: Confilict handling during simulation)
# 
# ## Update timeline
# 1. update first planned time to global timeline(#Reminder)
# 
# #Simulation and Optimization
# 1. on call from timeline : find next optimal solution if not increment delivery time and trigger an action 
# 2. on call from event : mark timeline as complete, update optimal solution, and create new entry in timeline

# %%

cease_continuous_run = threading.Event()
# insert digital twin ref by ID
# find digital twin ref by id and complete the event
class KafkaThread(threading.Thread):
    def __init__(self, dts):
        threading.Thread.__init__(self)
        self.dts = dts
    # @classmethod
    def run(self):
        consumer = KafkaConsumer('quickstart', auto_offset_reset='latest',
                     bootstrap_servers=['localhost:9092'], api_version=(0, 10))
        
        for msg in consumer:
            if bytes.decode(msg.key) == 'itemId':
                index = int(bytes.decode(msg.value))
                digitalTwins = self.dts
                if index in digitalTwins.index:
                    digitalTwin = digitalTwins.loc[index, 'ref']
                    print(isinstance(digitalTwin, DigitalTwin))
                    digitalTwin.markNodeAsComplete()

        if consumer is not None:
            consumer.close()


digitalTwins = pd.DataFrame(columns=['id', 'ref']).set_index('id')
supplyChainNodes = pd.DataFrame(columns=['id', 'ref']).set_index('id')

class SupplyChainEventListner(object):
    def __init__(self, digitalTwins):
        self.continuous_thread = KafkaThread(digitalTwins)
    def start(self):
        self.continuous_thread.start()

if __name__ == "__main__":
    nodes = ['n1', 'n2', 'n3']
    # itemDetails = [{
    #     'id': 12,
    #     'deliveryDate': datetime.now() + timedelta(seconds=68),
    #     'priority': 1,
    #     'routes': [
    #             {'nodeName': 'n1', 'processingTime': 4, 'transportTime': 2},
    #             {'nodeName': 'n2', 'processingTime': 6, 'transportTime': 4},
    #             {'nodeName': 'n3', 'processingTime': 4, 'transportTime': 4}]
    # },{
    #     'id': 13,
    #     'deliveryDate': datetime.now() + timedelta(seconds=24),
    #     'priority': 2,
    #     'routes': [
    #             {'nodeName': 'n1', 'processingTime': 2, 'transportTime': 4},
    #             {'nodeName': 'n2', 'processingTime': 5, 'transportTime': 8}]
    # }]
    itemDetails = [{
        'id': 1,
        'deliveryDate': datetime.now() + timedelta(seconds=72),
        'priority': 1,
        'routes': [
                {'nodeName': 'n1', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n2', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n3', 'processingTime': 6, 'transportTime': 4}]
    },{
        'id': 2,
        'deliveryDate': datetime.now() + timedelta(seconds=60),
        'priority': 1,
        'routes': [
                {'nodeName': 'n1', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n2', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n3', 'processingTime': 6, 'transportTime': 4}]
    },{
        'id': 3,
        'deliveryDate': datetime.now() + timedelta(seconds=48),
        'priority': 1,
        'routes': [
                {'nodeName': 'n1', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n2', 'processingTime': 6, 'transportTime': 4},
                {'nodeName': 'n3', 'processingTime': 6, 'transportTime': 4}]
    }]

    # %%
    for node in nodes:
        temp = pd.DataFrame(data=[[SupplyChainNode(node)]], index=[node], columns=digitalTwins.columns)
        supplyChainNodes = supplyChainNodes.append(temp)

    # Initialize stating point of the Route
    simOptEngine = SupplyChainSimOptEngine()
    for itemDetail in itemDetails:

        startingPoint = itemDetail['routes'][0]
        node = DigitalTwinNode(startingPoint['nodeName'], startingPoint['processingTime'], startingPoint['transportTime'], supplyChainNodes.loc[startingPoint['nodeName'], 'ref'])

        # Create Digital Twin for item
        digitalTwin = DigitalTwin(itemDetail['id'], node, itemDetail['deliveryDate'],itemDetail['priority'])

        # Construct route for the item
        for route in itemDetail['routes'][1:]:
            node.linkNode(DigitalTwinNode(route['nodeName'], route['processingTime'], route['transportTime'], supplyChainNodes.loc[route['nodeName'], 'ref']))
            node = node.link
        
        simOptEngine.onTwinCreate(digitalTwin)
        temp = pd.DataFrame(data=[[digitalTwin]], index=[itemDetail['id']], columns=digitalTwins.columns)
        digitalTwins = digitalTwins.append(temp)

    
    supplyChainEventListner = SupplyChainEventListner(digitalTwins)
    supplyChainEventListner.start()
    # %%

    while True:
        time.sleep(1)